import React from 'react'
import Navigation from './Navigation'
import Form from './Form';

const Header = ({ history, handleSubmit }) => {
  return (
    <div>
      <h1>SnapShot</h1>
      <Form history={history} handleSubmit={handleSubmit} />
      <Navigation />
    </div>
  )
}

export default Header